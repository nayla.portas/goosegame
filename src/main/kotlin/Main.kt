fun main(args: Array<String>) {
    val rules = setOf(BridgeRule(),
                        HotelRule(),
                        WellRule(),
                        MazeRule(),
                        PrisonRule(),
                        DeathRule(),
                        FinishGameRule(),
                        TwoForwardRule(),
                        PenaltyRule(),
                        StayInSpaceRule())

    val printableRules = Game(rules).getPrintableRules(65)
    printableRules.forEach(::println)
}