class Game(private val rules: Set<Rule>) {

    fun getPrintableRules(amount: Int) : List<String> {
        val rules = arrayListOf<String>()
        for(i in 1..amount) {
            val rule = getRule(i)
            if (rule != null)
                rules.add(rule.getDescription(i))
        }
        return rules
    }

    private fun getRule(number: Int): Rule? {
        return rules.find { it.appliesTo(number) }
    }
}