interface Rule {
    fun getDescription(number: Int) : String
    fun appliesTo(number: Int) : Boolean
}

internal class BridgeRule() : Rule {
    override fun getDescription(number: Int): String {
        return "The Bridge: Go to space 12"
    }

    override fun appliesTo(number: Int) = number == 6
}

internal class TwoForwardRule() : Rule {
    override fun getDescription(number: Int): String {
        return "Move two spaces forward"
    }

    override fun appliesTo(number: Int) = number % 6 == 0
}

internal class HotelRule : Rule {
    override fun getDescription(number: Int): String {
        return "The Hotel: Stay for (miss) one turn"
    }

    override fun appliesTo(number: Int) = number == 19
}

internal class WellRule : Rule {
    override fun getDescription(number: Int): String {
        return "The Well: Wait until someone comes to pull you out - they then take your place"
    }

    override fun appliesTo(number: Int) = number == 31
}

internal class MazeRule : Rule {
    override fun getDescription(number: Int): String {
        return "The Maze: Go back to space 39"
    }

    override fun appliesTo(number: Int) = number == 42
}

internal class PrisonRule : Rule {
    override fun getDescription(number: Int): String {
        return "The Prison: Wait until someone comes to release you - they then take your place"
    }

    override fun appliesTo(number: Int) = number in 50..55
}

internal class StayInSpaceRule : Rule {
    override fun getDescription(number: Int): String {
        return "Stay in space $number"
    }

    override fun appliesTo(number: Int) = true
}

internal class DeathRule : Rule {
    override fun getDescription(number: Int): String {
        return "Death: Return your piece to the beginning - start the game again"
    }

    override fun appliesTo(number: Int) = number == 58
}

internal class FinishGameRule : Rule {
    override fun getDescription(number: Int): String {
        return "Finish: you ended the game"
    }

    override fun appliesTo(number: Int) = number == 63
}

internal class PenaltyRule : Rule {
    override fun getDescription(number: Int): String {
        return "Move to space 53 and stay in prison for two turns"
    }

    override fun appliesTo(number: Int) = number > 63
}