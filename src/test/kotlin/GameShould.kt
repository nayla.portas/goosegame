import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class GameShould {
    private lateinit var game : Game
    private var rule : String? = null

    @Before
    fun setup() {
        val rules = setOf(BridgeRule(),
            HotelRule(),
            WellRule(),
            MazeRule(),
            PrisonRule(),
            DeathRule(),
            FinishGameRule(),
            TwoForwardRule(),
            PenaltyRule(),
            StayInSpaceRule())
        game = Game(rules)
    }

    @Test
    fun `have the Hotel Space Rule in nineteen Space`() {
        whenGetRule(19)
        thenHasTheRule(HOTEL_SPACE_RULE)
    }

    @Test
    fun `have the Well Space Rule in thirty-one Space`() {
        whenGetRule(31)
        thenHasTheRule(WELL_SPACE_RULE)
    }

    @Test
    fun `have the Maze Space Rule in forty-two Space`() {
        whenGetRule(42)
        thenHasTheRule(MAZE_SPACE_RULE)
    }

    @Test
    fun `have the Prison Space Rule in fifty to fifty-five Spaces`() {
        whenGetRule(50)
        thenHasTheRule(PRISON_SPACE_RULE)
        whenGetRule(51)
        thenHasTheRule(PRISON_SPACE_RULE)
        whenGetRule(52)
        thenHasTheRule(PRISON_SPACE_RULE)
        whenGetRule(53)
        thenHasTheRule(PRISON_SPACE_RULE)
        whenGetRule(54)
        thenHasTheRule(PRISON_SPACE_RULE)
        whenGetRule(55)
        thenHasTheRule(PRISON_SPACE_RULE)
    }

    @Test
    fun `have the Bridge Space Rule in six Space`() {
        whenGetRule(6)
        thenHasTheRule(BRIDGE_SPACE_RULE)
    }

    @Test
    fun `have the Regular Space Rule in some Regular Spaces`() {
        whenGetRule(2)
        thenHasTheRule("$STAY_IN_SPACE_RULE 2")
        whenGetRule(22)
        thenHasTheRule("$STAY_IN_SPACE_RULE 22")
        whenGetRule(59)
        thenHasTheRule("$STAY_IN_SPACE_RULE 59")
    }

    @Test
    fun `have the Two Forward Space Rule in multiples of six Spaces except six, forty-two and fifty-four`() {
        whenGetRule(12)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(18)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(24)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(30)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(36)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(48)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(60)
        thenHasTheRule(TWO_FORWARD_SPACE_RULE)

        whenGetRule(6)
        thenHasNotTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(42)
        thenHasNotTheRule(TWO_FORWARD_SPACE_RULE)
        whenGetRule(54)
        thenHasNotTheRule(TWO_FORWARD_SPACE_RULE)
    }

    @Test
    fun `have Death Rule in fifty-eight Space`() {
        whenGetRule(58)
        thenHasTheRule(DEATH_SPACE_RULE)
    }

    @Test
    fun `have Finish Game Rule in sixty-three Space`() {
        whenGetRule(63)
        thenHasTheRule(FINISH_GAME_SPACE_RULE)
    }

    @Test
    fun `have Penalty Rule in positions greater than sixty-three`() {
        whenGetRule(64)
        thenHasTheRule(PENALTY_RULE)
        whenGetRule(75)
        thenHasTheRule(PENALTY_RULE)
    }

    private fun assertRuleInSpace(rule: String?, expectedDescription: String) {
        assertThat(rule).isNotNull
        if(rule != null)
            assertThat(rule).isEqualTo(expectedDescription)

    }

    private fun assertRuleNotInSpace(rule: String?, expectedDescription: String) {
        assertThat(rule).isNotNull
        if(rule != null)
            assertThat(rule).isNotEqualTo(expectedDescription)
    }

    private fun whenGetRule(number: Int) {
        val rules = game.getPrintableRules(number)
        rule = rules[number-1]
    }

    private fun thenHasTheRule(expectedRule: String) {
        assertRuleInSpace(rule, expectedRule)
    }

    private fun thenHasNotTheRule(expectedRule: String) {
        assertRuleNotInSpace(rule, expectedRule)
    }

    companion object {
        const val HOTEL_SPACE_RULE = "The Hotel: Stay for (miss) one turn"
        const val WELL_SPACE_RULE = "The Well: Wait until someone comes to pull you out - they then take your place"
        const val MAZE_SPACE_RULE = "The Maze: Go back to space 39"
        const val PRISON_SPACE_RULE = "The Prison: Wait until someone comes to release you - they then take your place"
        const val BRIDGE_SPACE_RULE = "The Bridge: Go to space 12"
        const val STAY_IN_SPACE_RULE = "Stay in space"
        const val TWO_FORWARD_SPACE_RULE = "Move two spaces forward"
        const val DEATH_SPACE_RULE = "Death: Return your piece to the beginning - start the game again"
        const val FINISH_GAME_SPACE_RULE = "Finish: you ended the game"
        const val PENALTY_RULE = "Move to space 53 and stay in prison for two turns"
    }
}
